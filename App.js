/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import { Navigator, StatusBar, Platform, StyleSheet, Text, View, Alert, AsyncStorage } from 'react-native'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator, Header } from 'react-navigation-stack';

import LoginScreen from './src/components/loginForm/Login';
import CustomerForm from './src/components/customerForm/CustomerForm';
import CustomerDetails from './src/components/customerDetails/CustomerDetails';


const RootStack = createStackNavigator({
  LoginScreen: {
    screen: LoginScreen,
    navigationOptions: { header: null }

  },
  CustomerForm: {
    screen: CustomerForm,
    navigationOptions: { header: null }

  },
  CustomerDetails: {
    screen: CustomerDetails,
    navigationOptions: { header: null }
  }
},

  {
    initialRouteName: 'LoginScreen',
  },
  {
    headerMode: 'screen'
  },
);

const AppContainer = createAppContainer(RootStack);

export default class App extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <AppContainer />
      </View>
    );
  }
}




//----------------------------------------------------------------------------//

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});


