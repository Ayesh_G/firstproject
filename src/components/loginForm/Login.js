
import React, { Component } from 'react';
import {
  Platform, StyleSheet, Text, View, Image, Linking, TouchableOpacity,
  Alert, ImageBackground, StatusBar
} from 'react-native';
import { Form, Container, Item, Input, Button } from 'native-base';


export default class App extends Component {

  state = {
    name: '',
    password: ''
  }
  handleName = (text) => {
    this.setState({ name: text })
  }
  handlePassword = (text) => {
    this.setState({ password: text })
  }
  login = (name, pass) => {
    alert('Name : " ' + name + ' " / Password : " ' + pass + ' "')
  }
  validation() {
    // if (this.state.name == "com" && this.state.password == "123") {
    //   this.props.navigation.navigate('CustomerForm')

    // } else {
    //   if (this.state.name == "" || this.state.password == "") {
    //     Alert.alert("Empty Fields")
    //   } else {
    //     Alert.alert("username or password incorect")
    //   }
    // }
    this.props.navigation.navigate('CustomerForm');
  }
  render() {
    return (
      <Container style={styles.contain}>

        {/* <ImageBackground source={require('../../assests/last.jpg')} style={styles.backImg}> */}

        <Form style={styles.formTg}>

          <ImageBackground source={require('../../assests/billaBack.jpg')} style={styles.billaFrmBack}>

            <Image source={require('../../assests/billa.png')} style={styles.billa}></Image>

            <View style={styles.imgView}>
              <View style={styles.itmView}>

                <Item stackedLabel style={styles.itm}>

                  <Input
                    style={styles.input}
                    underlineColorAndroid="transparent"
                    placeholder="Username"
                    placeholderTextColor="#ffffff"
                    // autoCapitalize="none"
                    onChangeText={this.handleName}
                    
                  ></Input>

                </Item>

                <Item stackedLabel style={styles.itm}>

                  <Input
                    style={styles.input}
                    underlineColorAndroid="transparent"
                    placeholder="Password"
                    placeholderTextColor="#ffffff"
                    autoCapitalize="none"
                    onChangeText={this.handlePassword}
                    secureTextEntry={true}
                  ></Input>

                </Item>
              </View>
              <Button
                style={styles.Button}
                onPress={() => this.validation()}>
                <Text style={styles.ButtonText}> LOGIN </Text>
              </Button>

              <View style={styles.center}>

                <View style={styles.forgotHyperView}>
                  <Text style={styles.forgotHyper} onPress={() => Linking.openURL('https://')}>Forgot Your Password ?</Text>
                </View>

              </View>

            </View>

          </ImageBackground>
        </Form>

        <View style={styles.topView}>

          <View style={styles.create}>
          
            <Text style={styles.dont}>Not A Member?</Text>
            <Text style={styles.hyper} onPress={() => Linking.openURL('https://google.com')}>Become A member</Text>
          
          </View>

        </View>
        <View style={styles.background}>

          <StatusBar
            backgroundColor="transparent"
            barStyle="light-content"
            hidden={false}
            translucent={true}
          />

        </View>
        {/* </ImageBackground> */}
      </Container>
    );
  }
}


const styles = StyleSheet.create(
  {
    itmView: {
      alignItems:'center',
      left: -6
    },
    forgotHyperView: {
      marginTop: 70
    },
    forgotHyper: {
      color: '#ffffff',
      fontWeight: 'bold',
      fontSize: 18,
    },
    billaFrmBack: {
      width: '100%',
      height: '100%',
      alignItems: 'center'
    },
    imgView: {
      top: '30%',
      alignItems: 'center'
    },
    itm: {
      borderColor: 'transparent',
      marginBottom: 10,
      alignItems: 'center'
    },
    contain: {
      alignItems: 'center',
      height: '10%',
      overflow: 'hidden',
      backgroundColor: '#ffffff'
    },
    formTg: {
      // top: 25,
      top: 0,
      position: 'absolute',
      // backgroundColor: "#0C4056",
      width: '100%',
      height: '70%',
      borderBottomRightRadius: 60,
      // borderBottomLeftRadius: 40,
      borderColor: 'transparent',
      overflow: 'hidden',
    },
    billaView: {
      position: 'absolute',
      alignItems: 'center',
      width: '100%',
      backgroundColor: '#0C4056',
      shadowColor: "#000",
      shadowOpacity: 0.5,
      shadowRadius: 5,
      elevation: 10
    },
    billa: {
      top: '10%',
      width: '60%',
      height: '20%',
    },
    topView: {
      position: 'absolute',
      bottom: '20%',
      alignItems: 'center'
    },
    dont: {
      color: '#0C4056',
      fontSize: 18,
      fontWeight: 'bold',
      margin: 10
    },
    // background: {
    //   // backgroundColor: '#FFFFFF',
    //   width: '100%',
    //   height: '100%',
    // },
    background: {
      position: 'absolute',
      top: '5%',
      width: '100%',
      height: '100%',
      borderRadius: 40,
      alignItems: 'center'
    },
    // loginTXT: {
    //   fontSize: 55,
    //   fontWeight: 'bold',
    //   color: '#9D9797',
    //   left: '5%'
    // },
    // input: {
    //   top: 70,
    //   // margin: 15,
    //   height: 40,
    //   borderColor: '#9D9797',
    //   borderWidth: 1,
    //   borderRadius: 20,
    //   zIndex: 5,
    // },
    input: {
      borderColor: '#ffffff',
      borderWidth: 3,
      // borderRadius: 50,
      borderTopLeftRadius: 40,
      borderBottomRightRadius: 40,
      fontWeight: 'bold',
      margin: 3,
      padding: 10,
      // marginBottom: 10,
      fontSize: 18,
      textAlign: 'center',
      width: 400,
      color: '#ffffff',
    },
    center: {
      alignItems: "center",
      width: '100%'
    },
    login: {
      fontSize: 40,
      textAlign: 'center',
      color: '#3498db',
    },
    backImg: {
      width: '100%',
      height: '100%',
      bottom: 0,
      // position: 'absolute'
    },
    hyper: {
      color: '#F55F5D',
      fontSize: 18,
      fontWeight:'bold',
    },
    create: {
      alignItems: 'center'
    },
    Button: {
      top: '8%',
      backgroundColor: '#ffffff',
      // padding: 10,
      justifyContent: 'center',
      margin: 10,
      height: 50,
      // borderRadius: 50,
      borderTopLeftRadius: 35,
      borderBottomRightRadius: 35,
      width: 380,
      alignItems: "center",
      zIndex: 1,
    },
    ButtonText: {
      color: '#355F77',
      fontWeight: 'bold',
      fontSize: 25,
      letterSpacing: 1
    }
  }
)