import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, ImageBackground, Alert, AsyncStorage ,StatusBar} from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label, Button, Text, View } from 'native-base';
import ImagePicker from 'react-native-image-picker';

const options = {
  title: 'Choose Photo',
  takePhotoButtonTitle: 'Take photo with your camera',
  chooseFromlLibraryTitle: 'choose photo from library'
}

// const App = () => {
//   return (
//      <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
//   )
// }
// export default App


export default class Customer extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      address: '',
      contact: '',
      nic: '',
      avatarSource: null,
      pic: null
    };
  }

  saveData() {
    let obj = {
      Name : this.state.name,
      Address : this.state.address,
      Contact : this.state.contact,
      NIC : this.state.nic,
      
    }
    AsyncStorage.setItem('customer',JSON.stringify(obj));
    this.props.navigation.navigate('CustomerDetails');
  }

  nameREG(text) {
    const reg = /^[a-zA-Z ]*$/;
    if (reg.test(text) === false) {
      return false;
    } else {
      return true;
    }
  }

  nicREG(text) {
    const reg = /^([0-9]{9}[x|X|v|V]|[0-9]{12})$/
    if (reg.test(text) === false) {
      return false;
    } else {
      return true;
    }
  }

  nameValidation(t) {
    if(this.nameREG(t) == false){
      Alert.alert(this.state.name)
    }else{
      Alert.alert('Something Wrong')
    }
  }
  // Validation() {
  //   this.props.navigation.navigate('NavigateForm')
  // }
  myPhoto = () => {

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('Image Picker Error: ', response.error);
      }

      else {
        let source = { uri: response.uri };

        this.setState({
          avatarSource: source
        });
      }
    });
  }

  saveCustomer() {
 
    // Alert.alert(this.state.imageC)

    this.setState({
        spinner: true
    })

    var data = JSON.stringify({
        businessName: this.state.businessName,
        businessType: this.state.businessType,
        owner: this.state.owner,
        address: this.state.address,
        mobile: this.state.mobile,
        email: this.state.email,
        longitude: '0.23232',
        latitude: '0.2323232',
        firstImagePath: this.state.imageA,
        secondImagePath: this.state.imageB,
        thridImagePath: this.state.imageC,
    })

    fetch('https://salesdrive.commercialtp.com/api/clientDetail',
        {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                'Authorization': 'Basic ' + base64.encode(this.state.userName + ":" + this.state.password),

            },
            body: data
        }
    )

        .then(resp => resp.json())
        .then(responseJson => {
            console.log("save Customer" + JSON.stringify(responseJson));
            if (responseJson.status === 200) {

                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'DetailsScreen' })],
                });
                this.setState({
                    indicator: false
                })
                this.props.navigation.dispatch(resetAction);

                // this.props.navigation.navigate('DetailsScreen')
            } else {
                Alert.alert("customer save unsuccessful!")
            }

        })

        .catch(error => {
            console.log(error);
            spinner: false

        });
}


  render() {
    const { navigate } = this.props.navigation;
    return (

      <Container style={styles.contain}>

        <View style={styles.backView}>
          <ImageBackground style={styles.backImg} source={require('../../assests/billaBack.jpg')}>

            <View style={styles.head}>
              <Text style={styles.manage}>Manage</Text>
              <Text style={styles.cus}>Customer</Text>
              <Text style={styles.frm}>Form</Text>
            </View>

            <TouchableOpacity
              style={styles.touch}
              onPress={this.myPhoto}
            >
              {/* <Image source={this.state.avatarSource== null ? require('../../assests/addImg.png') : this.state.avatarSource} style={styles.add}></Image> */}
              <Image source={require('../../assests/addImg.png')} style={styles.add}></Image>
              <Image source={this.state.avatarSource} style={styles.upImg}></Image>

            </TouchableOpacity>

            <Form style={styles.formTg}>

              <View style={styles.frmView}>
                
                <Item stackedLabel style={styles.itm}>
                  <Input
                    style={styles.input}
                    placeholder="Customer Name"
                    placeholderTextColor="#9D9797"
                    value={this.state.name}
                    // onEndEditing={text => this.nameValidation(text)}
                    // onChangeText={this.handleName}
                    onChangeText={text => this.setState({ name: text })}
                  ></Input>
                </Item>
                <Item stackedLabel style={styles.itm}>
                  <Input
                    style={styles.input}
                    placeholder="Customer Address"
                    value={this.state.address}
                    placeholderTextColor="#9D9797"
                    onChangeText={text => this.setState({ address: text })}
                  ></Input>
                </Item>
                <Item stackedLabel style={styles.itm}>
                  <Input
                    style={styles.input}
                    placeholder="Customer Contact"
                    placeholderTextColor="#9D9797"
                    value={this.state.contact}
                    onChangeText={text => this.setState({ contact: text })}
                  ></Input>
                </Item>
                <Item stackedLabel style={styles.itm}>
                  <Input
                    style={styles.input}
                    placeholder="Customer NIC"
                    placeholderTextColor="#9D9797"
                    value={this.state.nic}
                    onChangeText={text => this.setState({ nic: text })}
                  ></Input>
                </Item>
                <View>
                  <Button block onPress={()=>this.saveData()}
                    style={styles.button}>
                    {/* onPress={
                      // () => this.submit(this.state.name, this.state.address, this.state.contact, this.state.nic)
                      () => this.Validation()
                    }

                    onPress={() =>  this.props.navigation.navigate('NavigateForm', {  name: this.state.name,address : this.state.address,contact: this.state.contact,nic:this.state.nic})  
                    } */}
                  

                    <Text style={styles.buttonFont}>submit</Text>
                  </Button>

                </View>

              </View>

            </Form>

          </ImageBackground>
        </View>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  contain: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  backView: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    borderRadius:40,
    overflow: 'hidden',

    // shadowColor: "#000",
    // shadowOpacity: 0.5,
    // shadowRadius: 5,
    // elevation: 10,
  },
  upImg: {
    width: 125,
    height: 125,
    position: 'absolute',
    borderRadius: 125
  },
  add: {
    width: 40,
    height: 40
  },
  touch: {
    width: 200,
    height: 200,
    borderRadius: 100,
    backgroundColor: '#ffffff',
    position: 'absolute',
    zIndex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    right: 20,
    top: '20%',
    shadowColor: "#000",
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10
  },
  input: {
    borderColor: '#9D9797',
    borderWidth: 1.5,
    borderTopLeftRadius: 30,
    borderBottomRightRadius: 30,
    margin: 8,
    fontSize: 15,
    textAlign: 'center',
    width: 400
  },
  itm: {
    borderColor: 'transparent'
  },
  frmView: {
    top: '25%',
    alignItems: 'center',
  },
  formTg: {
    // top: 25,
    bottom: -1,
    position: 'absolute',
    backgroundColor: "#ffffff",
    width: '100%',
    height: '75%',
    borderTopLeftRadius: 40,
    borderBottomRightRadius: 40,
    alignItems: 'center',
    borderColor: 'transparent'
  },
  head: {
    top: '5%',
    left: '5%'
  },
  manage: {
    // top: 10,
    fontSize: 30,
    color: '#ffffff',
    fontWeight: 'bold'
  },
  cus: {
    fontSize: 50,
    color: '#ffffff',
    fontWeight: 'bold'
  },
  frm: {
    // bottom: 10,
    fontSize: 30,
    color: '#ffffff',
    fontWeight: 'bold'
  },
  button: {
    width: 300,
    height: 55,
    borderTopLeftRadius: 30,
    borderBottomRightRadius: 30,
    marginTop: 70,
    backgroundColor: '#355F77'
  },
  buttonFont: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  form: {
    marginTop: 20
  },
  Image: {
    borderColor: "#C6C6C6",
    borderRadius: 100,
    borderWidth: 1,
    width: 150,
    height: 150,
    left: 105,
    top: 30,
    backgroundColor: "#FFFFFF"
  },
  backImg: {
    width: '100%',
    height: '100%',
  }
});