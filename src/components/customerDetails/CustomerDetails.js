import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, AsyncStorage, Image, FlatList } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import base64 from 'react-native-base64';

export default class CustomerDetails extends Component {
  state = {
    name: '',
    address: '',
    contact: '',
    nic: '',
    userName: 'Vidura',
    password: '1234',
    data: []
  }


  componentWillMount() {
    this.gettingCustomers()
  }



  gettingCustomers() {
    fetch("http://5.189.148.181:8080/sales-drive/api/clientDetail?text=&count=20&page=0 ",
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          'Authorization': 'Basic ' + base64.encode(this.state.userName + ":" + this.state.password),
        }
      }
    )
      .then(response => {
        console.log(JSON.stringify(response))
        return response.json()
      })
      .then(responseJson => {
        console.log(JSON.stringify(responseJson))
        this.setState({
          data: responseJson
        })

      })
      .catch((error) => {
        console.log(error);

      });
  }

  saveCustomer() {
 
    // Alert.alert(this.state.imageC)

    this.setState({
        spinner: true
    })

    var data = JSON.stringify({
       name: this.state.name,
       address: this.state.address,
       contact: this.state.contact,
       nic: this.state.nic,
    })

    fetch('https://salesdrive.commercialtp.com/api/clientDetail',
        {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                'Authorization': 'Basic ' + base64.encode(this.state.userName + ":" + this.state.password),

            },
            body: data
        }
    )

        .then(resp => resp.json())
        .then(responseJson => {
            console.log("save Customer" + JSON.stringify(responseJson));
            if (responseJson.status === 200) {

                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'DetailsScreen' })],
                });
                this.setState({
                    indicator: false
                })
                this.props.navigation.dispatch(resetAction);

                // this.props.navigation.navigate('DetailsScreen')
            } else {
                Alert.alert("customer save unsuccessful!")
            }

        })

        .catch(error => {
            console.log(error);
            spinner: false

        });
}

  render() {
    return (
      <View style={styles.View}>
        <FlatList
          data={this.state.data}
          renderItem={({ item }) =>
            <View style={{ backgroundColor: 'White', height: 70, width: 300,marginBottom:10,marginTop:10,
             shadowColor: "White",
             shadowOffset: {
                 width: 0,
                 height: 10,
             },
             shadowOpacity: 0.53,
             shadowRadius: 15.98,
             elevation: 21,}}>
              <Text style={styles.item} style={{color:"black", left:20, top:20,fontSize:20}}>{item.businessName}</Text>
              <Text style={styles.item} style={{color:"black",left:200}}>{item.id}</Text>
              <Image style={{ width: 60, height: 60 , borderRadius:50, right:0 , position:"absolute"}} source={{ uri: 'http://5.189.148.181:8080/sales-drive/' + item.firstImagePath }}></Image>
            </View>
          }

        />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  View: {
    backgroundColor: 'white',
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",
    height: hp('96%'),
    width: wp('100%')
  },
  view1: {
    backgroundColor: "white",
    width: 320,
    height: 320,
    borderRadius: 50,
    shadowColor: "#F9B74F",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.53,
    shadowRadius: 15.98,
    elevation: 21,
  },
  img: {
    width: 260,
    top: 0,
    right: 0,
    position: "absolute",
  },
  backArrow: {
    position: "absolute",
    top: 10,
    left: 20
  },
  text: {
    position: "absolute",
    right: 30,
    top: 1e0,
    fontSize: 50,
    fontWeight: "bold",
    color: "white"
  },
  textStyle: {
    position: "absolute",
    fontSize: 23,
    textAlign: 'center',
    color: '#333333',
    marginTop: 10,
    top: 20,
    left: 50
  },
  textStyle1: {
    position: "absolute",
    fontSize: 23,
    textAlign: 'center',
    color: '#333333',
    marginTop: 10,
    top: 90,
    left: 50
  },
  textStyle2: {
    position: "absolute",
    fontSize: 23,
    textAlign: 'center',
    color: '#333333',
    marginTop: 10,
    top: 160,
    left: 50
  },

  textStyle3: {
    position: "absolute",
    fontSize: 23,
    textAlign: 'center',
    color: '#333333',
    marginTop: 10,
    top: 230,
    left: 50
  },
  img1: {
    width: 200,
    bottom: -10,
    left: 0,
    position: "absolute",
    rotation: 180
  },
});