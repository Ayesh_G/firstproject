import React, { Component } from 'react'
import { View, TouchableOpacity, Text, Image, StyleSheet, ImageBackground, AsyncStorage, TextInput, Alert, FlatList, NetInfo, ActivityIndicator, } from 'react-native'
import { Container, Header, Content, Card, CardItem, Body, Button, Fab, Icon, Label, Badge, } from 'native-base';
import DraggableFlatList from 'react-native-draggable-flatlist';
import base64 from 'react-native-base64';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import Spinner from 'react-native-loading-spinner-overlay';
import { FlatGrid } from 'react-native-super-grid';
import LinearGradient from 'react-native-linear-gradient'
import Strings from '../../resources/Strings';
 
export default class Client_tabOne extends Component {
 
    static navigationOptions = {
        title: 'Client List',
    }
 
    constructor(props) {
        super(props);
        this.state = {
 
            clientName: '',
            clientNic: '',
            clientMobile: '',
            clientLand: '',
            clientEmail: '',
            clientSerial: '',
            clientUsername: '',
            clientPassword: '',
            clientAddress: '',
            clientImage: '',
            pendingData: [],
 
            userName: '',
            password: '',
 
            isLoading: true,
            renderStatus: false,
            isFetching: false,
            indicator: false,
            Spinner: false,
 
            data: []
        };
    }
 
    onRefresh() {
 
        this.setState({ isFetching: true }, function () { this.loadPending() });
    }
 
    componentWillMount() {
        this.validRender();
        this.showData();
        this.setState({
            Spinner: true
        })
 
    }
 
    showData = async () => {
        let myArray = await AsyncStorage.getItem(Strings.USER_DATA);
        let d = JSON.parse(myArray);
 
        this.setState({
            userName: d.username,
            password: d.password,
 
        })
        console.log("response :" + this.state.userName)
        console.log("response :" + this.state.password)
        this.loadPending();
    }
 
 
    loadPending() {
        this.setState({
            indicator: true
        })
        console.log("responseasa :" + this.state.userName)
        console.log("responseasas :" + this.state.password)
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL+"/agent/client/confirm",
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': 'Basic ' + base64.encode(this.state.userName + ":" + this.state.password),
                        }
                    }
                )
                    .then(response => {
                        console.log(JSON.stringify(response))
                        return response.json()
                    })
                    .then(responseJson => {
 
 
                        this.setState({
                            data: responseJson,
                            // Spinner:false
 
                        });
                        console.log(JSON.stringify(this.state.data))
                        this.setState({
                            Spinner: false,
                            renderStatus: false,
                            isFetching: false,
                            indicator: false
 
                        })
                        this.setState({ isFetching: false })
                        for (let index = 0; index < this.state.pendingData.length; index++) {
 
                            console.log("Pending:>:>:>" + JSON.stringify(this.state.pendingData[index]))
 
                        }
                        console.log("Pending:>:>:>" + JSON.stringify(this.state.data))
 
                    })
                    .catch((error) => {
                        console.log(error);
                        this.setState({
                            spinner: false,
 
                        })
                    
 
                    });
            } else {
                this.setState({
                    renderStatus: true,
                    isFetching: false,
                    indicator: false
                })
                showMessage({
                    message: "Connection Failed",
                    description: Strings.CHECK_NETWORK_CONNECTION,
                    type: "danger",
                    icon: "danger",
                    color: "black", 
                });
            }
        });
    }
 
    indicator() {
        if (this.state.indicator) {
 
            return (
                <View style={{ justifyContent: 'center', alignItems: 'center', top: 10 }}>
                    <ActivityIndicator size="large" color="#54A759" />
                </View>
            )
        }
    }
 
    validRender() {
        if (this.state.renderStatus) {
 
            return (
         
                <View style={{ justifyContent: 'center', alignItems: 'center', top: -60, height: 550 }}>
                    <Text style={{ textAlign: 'center' }}>Connection Problem !</Text>
                    <Text style={{ textAlign: 'center' }}>Please turn on your mobile data or WIFI network</Text>
                    <Text style={{ textAlign: 'center', marginBottom: 20 }}>and Try again.</Text>
                    <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                        <Button style={{ backgroundColor: '#1d913b', width: 100, height: 40, justifyContent: "center" }} onPress={() => this.loadPending()}>
                            <Text style={{ fontWeight: 'bold', color: 'white' }}>Retry</Text>
                        </Button>
                    </View>
                </View>
               
            )
        }
    }
 
    renderItem = ({ item, index, move, moveEnd, isActive }) => {
        return (
            <Content padder>
             
                <Card style={{ marginBottom: -12 }}>
                    <TouchableOpacity
                        style={{
                            height: 90,
                            backgroundColor: isActive ? 'blue' : item.backgroundColor,
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                        onLongPress={move}
                        onPressOut={moveEnd}
                    >
                        <Image style={{ height: 55, width: 55, borderRadius: 100, left: -130, top: 37 }}
                            source={{ uri: Strings.IMAGE_DOWNLOAD_URL + item.imageUrl }}
                        />
 
                        <Text style={{
                            top: -13,
                            color: 'black',
                            fontSize: 17,
                        }}>{item.name}</Text>
 
 
                        <Text style={{
                            top: -13,
                            fontWeight: 'bold',
                            color: 'black',
                            fontSize: 17,
                        }}>{item.mobile}</Text>
 
                        <Badge style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            right: -240,
                            bottom: 48,
                            width: 85,
                            height: 35,
                            backgroundColor: '#E74C3C',
                            borderRadius: 50
 
                        }}>
                            <Text style={{
                                color: 'white',
                            }}>
                                Pending
                            </Text>
 
                        </Badge>
 
                    </TouchableOpacity>
 
                </Card>
 
            </Content>
 
        )
    }
 
    render() {
 
        return (
 
          
            <Container>
                {this.indicator()}
 
                <FlatGrid
 
                    itemDimension={300}
                    items={this.state.data}
                    style={styles.gridView}
                    onRefresh={() => this.onRefresh()}
                    refreshing={this.state.isFetching}
                   
                    renderItem={({ item, index }) => (
                        <LinearGradient colors={['#2ECC71', '#239B56', '#1D8348']} style={styles.linearGradient}>
                            <View style={[styles.itemContainer, {}]}>
                                <View style={styles.itemStyle}>
                                    <Text style={[styles.itemName, {}]}>
 
                                        {item.name}</Text>
                                    <Text style={styles.itemCode}>
                                        <Image
                                            source={require("../assets/icons8-phone-32.png")}
                                            style={styles.ImageIconStyle}
                                        />
                                        <Label> </Label>
                                        {item.mobile}</Text>
                                    <Text style={styles.itemCode}>
                                        <Image
                                            source={require("../assets/icons8-new-post-32.png")}
                                            style={styles.ImageIconStyle}
                                        />
                                        <Label> </Label>
                                        {item.email}</Text>
                                    <Text style={styles.itemCode}>
                                        <Image
                                            source={require("../assets/icons8-address-48.png")}
                                            style={styles.ImageIconStyle}
                                        />
                                        <Label> </Label>
                                        {item.address}</Text>
                                    <Text style={styles.itemCode}>
                                        <Image
                                            source={require("../assets/icons8-user-32.png")}
                                            style={styles.ImageIconStyle}
                                        />
                                        <Label> </Label>
                                        {item.nic}</Text>
 
                                </View>
 
                                <Image style={styles.cardImage}
                                    source={{ uri: Strings.IMAGE_DOWNLOAD_URL + item.imageUrl }}
                                />
 
 
                            </View>
                        </LinearGradient>
 
                    )}
                />
                {this.validRender()}
 
            </Container>
        );
    }
}
 
const styles = StyleSheet.create({
 
    itemStyle: {
        marginLeft: 133,
        marginTop: 6 
    },
    cardImage: {
        // height: 115, width: 115, borderRadius: 10, marginLeft: 17,marginTop:17, position:"absolute",
        height: 90, width: 90, borderRadius: 10, marginLeft: 28, marginTop: 20.3, position: "absolute",
 
    },
 
    ImageIconStyle: {
        marginLeft: 10,
        height: 11,
        width: 11,
        resizeMode: "stretch"
    },
 
    backgroundImage: {
        width: "100%",
        height: 700
    },
 
    gridView: {
        marginTop: 10,
        flex: 1,
 
    },
    itemContainer: {
        borderRadius: 5,
        padding: 10,
        height: 161,
        width: 350,
        position: 'absolute',
        marginTop: -6,
        marginLeft: -5,
        // backgroundColor:  '#54A759',
 
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOpacity: 0.8,
        elevation: 6,
        shadowRadius: 15,
        shadowOffset: { width: 1, height: 13 },
    },
    itemName: {
 
        fontSize: 16,
        color: 'black',
        fontWeight: '600',
 
    },
    itemCode: {
        fontWeight: '100',
        fontSize: 15,
        color: 'black',
    },
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5,
        height: 120
    },
});